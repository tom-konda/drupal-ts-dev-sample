const path = require('path');
const coreDir = path.resolve(__dirname, './docroot/core');
const prettierOptions = require(`${coreDir}/.prettierrc`);

module.exports = prettierOptions;