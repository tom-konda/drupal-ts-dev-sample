const glob = require('glob');
const util = require('util');
const asyncGlob = util.promisify(glob);
const babelCore = require('@babel/core');
const path = require('path');
const fs = require('fs');


const transpile = async () => {
  const coreDir = path.resolve(__dirname, '../../docroot/core');
  const {babel} = require(`${coreDir}/package`);

  // Import Drupal core preset settings.
  for (let presetName in babel.env) {
    const [, preset] = babel.env[presetName].presets[0];
    babel.env[presetName].presets = [
      '@babel/preset-typescript',
      ['@babel/preset-env', preset],
    ];
  }

  babel.env.es6 = {
    presets: [
      '@babel/preset-typescript',
    ]
  }

  smartPhonePreset = {
    'targets': {
      'browsers': [
        'last 2 iOS major versions',
        'last 2 ChromeAndroid version',
      ]
    }
  }
  babel.env.smp = {
    presets: [
      '@babel/preset-typescript',
      ['@babel/preset-env', smartPhonePreset],
    ]
  }
  const match = './docroot/{modules,themes}/custom/**/ts/*.ts';
  const filePaths = await asyncGlob(match);
  filePaths.forEach(
    async (filePath) => {
      const {dir, name} = path.parse(filePath);
      const jsDir = path.resolve(dir, '../js');
      const outputFilePath = `${jsDir}/${name}.${process.env.BABEL_ENV}.js`;

      const result = await babelCore.transformFileAsync(
        filePath,
        {
          sourceMaps: process.env.NODE_ENV === 'development' ? true : false,
          comments: process.env.NODE_ENV === 'development' ? true : false,
          env: babel.env,
          plugins: [
          ],
        }
      )

      if (fs.existsSync(jsDir) === false) {
        await fs.promises.mkdir(jsDir);
      }

      await fs.promises.writeFile(outputFilePath, result.code)
    }
  )
}

transpile().catch(
  (error) => console.error(error)
)