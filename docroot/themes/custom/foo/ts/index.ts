(() => {
  const powered = 2 ** 2;
  const hoge = {...{foo: ''}, ...{bar: 1}};
  console.log(hoge.bar);
  powered.toFixed(2);
  const foo = [...[''], ...[0]];
  const xyzzy = (num: number, bool: boolean, ...args: Array<string>) => {
    if (bool) {
      return num;
    }
    const [first, ...rest] = args
    return {
      first,
      restParams: rest,
    }
  }
  console.log(foo);
  console.log(xyzzy(0, false, 'hogehoge', 'foobar'));
  const testObj = {
    a: null
  }
  const nullishTest = testObj.a ?? 'foo';
  console.log(nullishTest);
})();
